package com.example.dokusample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.doku.sdkocov2.DirectSDK
import com.doku.sdkocov2.interfaces.iPaymentCallback
import com.doku.sdkocov2.model.PaymentItems
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var dokuDirectSDk: DirectSDK? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_submit.setOnClickListener {

            connectDoku()
        }
    }

    private fun connectDoku() {

        dokuDirectSDk = DirectSDK()
        val paymentItems = PaymentItems()
        paymentItems.dataAmount = "33000.00"
        paymentItems.dataBasket = "[{\"name\":\"Pepsodent Toothpaste White 75 g\",\"amount\":\"4100.00\",\"quantity\":\"1\",\"subtotal\":\"4100.00\"},{\"name\":\"Promag Gazero Box 6 Sachet\",\"amount\":\"13900.00\",\"quantity\":\"1\",\"subtotal\":\"13900.00\"}]"
        paymentItems.dataCurrency = "360"
        paymentItems.dataWords = "33197ad785cbed1e6410ee1dbe9b065f2f47886a"
        paymentItems.dataMerchantChain = "NA"
        paymentItems.dataSessionID = "a3j6vtko9"
        paymentItems.dataTransactionID = "H15864211876191"
        paymentItems.dataMerchantCode = "6834"
        paymentItems.dataImei = "2fbd258a-16e0-4d59-82b1-b3d66eae6e8e"
        paymentItems.mobilePhone = "9632589654"
        paymentItems.publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv1jK7V1NR4Yq+bnnOYsBJtrtiP0Ac9ULuzHuBnfhCh1fX1LuoefNHGPPwJ6XlxRMdwDKAd7rvdJ2U1TcCEgzKVHGWf3zyP+qBKDPKvL3cHEeEvI/biN2UbSLGyMID+aimNkQSiAErzU0JoxvUfD8K5tYkVl+sgU5Y7cZhnhIoKxqvP4lzmvua3/dXYPcftEDHg6t59PRL6uGFGT+Rl0mztW0VlnZ4ugscpEb8Fgl4kq2KW1tNzSmzTRlC1xCILIcVI6Zl0CLKEkfDdqP6cAFfvoOnUvEya5s1/rzRU0XDwgv6RLAYRV3+wCFgR4lhxxIqB26XD5gSF473vW0piaCtwIDAQAB"
        paymentItems.isProduction(false)
        dokuDirectSDk!!.setCart_details(paymentItems)

        dokuDirectSDk!!.setPaymentChannel(15)

        dokuDirectSDk!!.getResponse(object : iPaymentCallback {
            override fun onSuccess(s: String) {
                Log.d("doku", "onSuccess $s")
            }

            override fun onError(s: String) {
                Log.d("doku", "onError $s")
            }

            override fun onException(e: Exception) {
                e.printStackTrace()
            }
        }, applicationContext)

    }
}
